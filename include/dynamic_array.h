#ifndef DYNAMIC_ARRAY_H
#define DYNAMIC_ARRAY_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

typedef struct Array Array;

Array *array_new(void);
void array_delete(Array *array);
bool array_append(Array *array, int64_t value);
bool array_prepend(Array *array, int64_t value);
bool array_insert(Array *array, size_t position, int64_t value);
bool array_expand(Array *array, size_t length, int64_t value);
size_t array_get_length(Array *array);
bool array_get_value(Array *array, size_t position, int64_t *value);
void array_clear(Array *array);
bool array_pop_back(Array *array, int64_t *value);
bool array_pop_front(Array *array, int64_t *value);
bool array_remove(Array *array, size_t position, int64_t *value);
bool array_shrink(Array *array, size_t length);

#endif /* DYNAMIC_ARRAY_H */


