#!/bin/sh
if [ "$#" -ne 3 ]; then
	echo "Usage: $0 <program_name> <input_file> <output_file>"
	exit 1
fi

PROGRAM=$1
INPUT=$2
OUTPUT=$3
$PROGRAM < $INPUT | diff -su - $OUTPUT
