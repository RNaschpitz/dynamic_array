#include "dynamic_array.h"
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>

struct Array {
	size_t length;
	int64_t *arr;
};

// Allocate space for the dynamic array structure with zero elements. If there's
// no memory for the allocation returns NULL.
Array *array_new(void)
{
	Array *array;

	array = malloc(sizeof(Array));

	if (array == NULL) {
		return NULL;
	}

	array->length = 0;
	array->arr = NULL;
	return array;
}

// Deallocate all memory used by the dynamic array.
void array_delete(Array *array)
{
	free(array->arr);
	free(array);
}

// Append a value at the end of the dynamic array. Returns true on success or
// false if there's not enough memory.
bool array_append(Array *array, int64_t value)
{
	int64_t *tmp;

	tmp = realloc(array->arr, (array->length + 1) * sizeof(int64_t));

	if (tmp != NULL) {
		array->arr = tmp;
	} else {
		return false;
	}

	array->arr[array->length++] = value;

	return true;
}

// Prepend a value at the beginning of the dynamic array. Returns true on
// success or false if there's not enough memory.
bool array_prepend(Array *array, int64_t value)
{
	int64_t *tmp;
	size_t i;

	tmp = realloc(array->arr, (array->length + 1) * sizeof(int64_t));

	if (tmp != NULL) {
		array->arr = tmp;
	} else {
		return false;
	}

	array->length++;

	for (i = array->length - 1; i > 0; i--) {
		array->arr[i] = array->arr[i - 1];
	}

	array->arr[0] = value;
	return true;
}

// Insert a value at the specified position in the dynamic array. The position
// may be an existing position on the array, or the position immediatelly
// following the last element of the array. Returns true on success, or false,
// either if the position is outside the array's bounds or there's not enough
// memory.
bool array_insert(Array *array, size_t position, int64_t value)
{
	int64_t *tmp;
	size_t i;

	if (array->length < position) {
		return false;
	} else if (array->length == position) {
		return array_append(array, value);
	} else if (position == 0) {
		return array_prepend(array, value);
	} else {
		tmp = realloc(array->arr, (array->length + 1) * sizeof(int64_t));
		if (tmp != NULL) {
			array->arr = tmp;
		} else {
			return false;
		}
	}

	array->length++;

	for (i = array->length - 1; i > position; --i) {
		array->arr[i] = array->arr[i - 1];
	}

	array->arr[position] = value;

	return true;
}

// Increase the size of the array to the specified length, filling the new
// elements with the given value. Returns true on success or false either if the
// requested length is not larger than the current array length, or if there's
// not enough memory.
bool array_expand(Array *array, size_t length, int64_t value)
{
	int64_t *tmp;
	size_t i;

	if (array->length >= length) {
		return false;
	}

	tmp = realloc(array->arr, length * sizeof(int64_t));
	if (tmp != NULL) {
		array->arr = tmp;
	} else {
		return false;
	}

	for (i = array->length; i < length; i++) {
		array->arr[i] = value;
	}

	array->length = length;
	return true;
}

// Returns the number of elements in the dynamic array.
size_t array_get_length(Array *array)
{
	return array->length;
}

// Gets a value in the dynamic array at the specified position. Returns true on
// success, or false if the position is out of bounds. The requested value is
// returned on the reference variable "value".
bool array_get_value(Array *array, size_t position, int64_t *value)
{
	if (array->length == 0) {
		return false;
	}

	if (array->length - 1 < position) {
		return false;
	}

	*value = array->arr[position];
	return true;
}

// Remove all the elements from the array, so that it's empty.
void array_clear(Array *array)
{
	free(array->arr);
	array->arr = NULL;
	array->length = 0;
}

// Remove the last element from the array and return its value. Returns true on
// success, or false if the array was empty before removal. The requested value
// is returned on the reference variable "value".
bool array_pop_back(Array *array, int64_t *value)
{
	if (array->length == 0) {
		return false;
	}
	else if (array->length == 1) {
		*value = array->arr[array->length - 1];
		array_clear(array);
		return true;
	}

	*value = array->arr[array->length - 1];
	int64_t *tmp = realloc(array->arr, (array->length - 1) * sizeof(int64_t));

	if (tmp != NULL) {
		array->arr = tmp;
	}

	array->length--;

	return true;
}

// Remove the first element from the array and return its value. Returns true on
// success, or false if the array was empty before removal. The requested value
// is returned on the reference variable "value".
bool array_pop_front(Array *array, int64_t *value)
{
	size_t i;

	if (array->length == 0) {
		return false;
	}
	else if (array->length == 1) {
		*value = array->arr[0];
		array_clear(array);
		return true;
	}

	*value = array->arr[0];

	for (i = 0; i < array->length - 1; ++i) {
		array->arr[i] = array->arr[i + 1];
	}

	int64_t *tmp = realloc(array->arr, (array->length - 1) * sizeof(int64_t));

	if (tmp != NULL) {
		array->arr = tmp;
	}

	array->length--;

	return true;
}

// Remove the element from the array in the specified position and return its
// value. Returns true on success, or false if the position is out of bounds.
// The requested value is returned on the reference variable "value".
bool array_remove(Array *array, size_t position, int64_t *value)
{
	int64_t *tmp;
	size_t i;

	if (array->length == 0) {
		return false;
	}
	if (array->length - 1 < position) {
		return false;
	}
	if (array->length - 1 == position) {
		return array_pop_back(array, value);
	} else if (position == 0) {
		return array_pop_front(array, value);
	}

	*value = array->arr[position];

	for (i = position; i < array->length - 1; ++i) {
		array->arr[i] = array->arr[i + 1];
	}
	tmp = realloc(array->arr, (array->length - 1) * sizeof(int64_t));

	if (tmp != NULL) {
		array->arr = tmp;
	}

	array->length--;

	return true;
}

// Reduces the length of the array to the given length by deleting the elements
// in the array that are beyond the requested length. Returns true on success or
// false if the requested length is not smaller than the current array length.
bool array_shrink(Array *array, size_t length)
{
	int64_t *tmp;

	if (array->length <= length) {
		return false;
	}
	else if (length == 0) {
		array_clear(array);
	}

	tmp = realloc(array->arr, length * sizeof(int64_t));

	if (tmp != NULL) {
		array->arr = tmp;
	}

	array->length = length;

	return true;
}
